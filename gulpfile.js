'use strict';

var gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    prefixer    = require('gulp-autoprefixer'),
    uglify      = require('gulp-uglify'),
    less        = require('gulp-less'),
    $path       = require('path'),
    concat      = require('gulp-concat'),
    rigger      = require('gulp-rigger'),
    cssmin      = require('gulp-minify-css'),
    imagemin    = require('gulp-imagemin'),
    zip         = require('gulp-zip'),
    spritesmith = require('gulp.spritesmith'),
    pngquant    = require('imagemin-pngquant'),
    rimraf      = require('rimraf'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload;

var path = {
	build: { //Тут мы укажем куда складывать готовые после сборки файлы
		html     : 'build/',
		js       : 'build/js/',
		css      : 'build/css/',
		img      : 'build/i/',
		bg_images: 'build/i/bg/',
		sprite   : 'build/i/bg/',
		fonts    : 'build/fonts/'
	},
	src  : { //Пути откуда брать исходники
		html  : 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
		js    : 'src/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
		style : 'src/style/main.less',
		img   : ['./src/i/**/*.*', '!./src/i/sprite/**/*.*'], //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
		sprite: 'src/i/sprite/**/*.*',
		fonts : 'src/fonts/**/*.*'
	},
	watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
		html  : 'src/**/*.html',
		js    : 'src/js/**/*.js',
		style : 'src/style/**/*.less',
		img   : ['src/i/**/*.*', '!./src/i/sprite/**/*.*'],
		sprite: 'src/i/sprite/**/*.*',
		fonts : 'src/fonts/**/*.*'
	},
	clean: './build'
};

var config = {
	server   : {
		baseDir: "./build"
	},
	tunnel   : false,
	host     : 'localhost',
	port     : 9000,
	logPrefix: "EsterFurs"
};

gulp.task('style:build', function ()
{
	return gulp.src(path.src.style)
		.pipe(less({
			paths: [$path.join(__dirname, 'less')]
		}))
		.pipe(prefixer()) //Добавим вендорные префиксы
//		.pipe(cssmin()) //Сожмем
		.pipe(gulp.dest(path.build.css)) //И в build
		.pipe(reload({stream: true})); // даем команду на перезагрузку
});

gulp.task('html:build', function ()
{
	gulp.src(path.src.html) //Выберем файлы по нужному пути
		.pipe(rigger()) //Прогоним через rigger
		.pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
		.pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function ()
{
	gulp.src(path.src.js) //Найдем наш main файл
		.pipe(rigger()) //Прогоним через rigger
//		.pipe(uglify()) //Сожмем наш js
		.pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
		.pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('image:build', function ()
{
	gulp.src(path.src.img) //Выберем наши картинки
		.pipe(imagemin({ //Сожмем их
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use        : [pngquant()],
			interlaced : true
		}))
		.pipe(gulp.dest(path.build.img)) //И бросим в build
		.pipe(reload({stream: true}));
});

gulp.task('fonts:build', function ()
{
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
});

gulp.task('bower', function ()
{
	gulp.src('./bower_components/bootstrap/dist/js/bootstrap.js')
//		.pipe(uglify())
		.pipe(concat('bootstrap.min.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
		.pipe(gulp.dest(path.build.js));

	gulp.src('./bower_components/bootstrap/fonts/**/*.*')
		.pipe(gulp.dest(path.build.fonts + 'bootstrap/'));

	gulp.src('./bower_components/fontawesome/fonts/**/*.*')
		.pipe(gulp.dest(path.build.fonts + 'fontawesome/'));

	gulp.src('./bower_components/bxSlider/src/images/**/*.*')
		.pipe(gulp.dest(path.build.bg_images + 'bxslider/'));

	/* fancybox */
	gulp.src(['./bower_components/fancybox/source/*.png','./bower_components/fancybox/source/*.gif'])
		.pipe(gulp.dest(path.build.css + 'fancybox/'));
	gulp.src(['./bower_components/fancybox/source/helpers/*.png','./bower_components/fancybox/source/helpers/*.gif'])
		.pipe(gulp.dest(path.build.css + 'fancybox/helpers/'));

	gulp.src('./bower_components/fancybox/source/*.css')
		.pipe(gulp.dest(path.build.css + 'fancybox/'));
	gulp.src('./bower_components/fancybox/source/helpers/*.css')
		.pipe(gulp.dest(path.build.css + 'fancybox/helpers/'));

	gulp.src('./bower_components/fancybox/source/*.js')
		.pipe(gulp.dest(path.build.js + 'fancybox/'));
	gulp.src('./bower_components/fancybox/source/helpers/*.js')
		.pipe(gulp.dest(path.build.js + 'fancybox/helpers/'));
});

gulp.task('sprite', function ()
{
	var spriteData =
		    gulp.src(path.src.sprite) // путь, откуда берем картинки для спрайта
			    .pipe(spritesmith({
				    imgName    : 'sprite.png',
				    cssName    : 'sprite.less',
				    algorithm  : 'top-down',//'binary-tree',
				    imgPath    : '../i/bg/sprite.png',
				    cssTemplate: 'less.template.spritesmith',
				    cssVarMap  : function (sprite)
				    {
					    sprite.name = 'b-icon__' + sprite.name
				    }
			    }));

	spriteData.img.pipe(gulp.dest(path.build.sprite)); // путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest('./src/style/')); // путь, куда сохраняем стили
});
gulp.task('build', [
	'html:build',
	'js:build',
	'style:build',
	'fonts:build',
	'image:build',
	'bower'
]);

gulp.task('watch', function ()
{
	watch([path.watch.html], function (event, cb)
	{
		gulp.start('html:build');
	});
	watch([path.watch.sprite], function (event, cb)
	{
		gulp.start('sprite');
	});
	watch([path.watch.style], function (event, cb)
	{
		gulp.start('style:build');
	});
	watch([path.watch.js], function (event, cb)
	{
		gulp.start('js:build');
	});
	watch(path.watch.img, function (event, cb)
	{
		gulp.start('image:build');
	});
	watch([path.watch.fonts], function (event, cb)
	{
		gulp.start('fonts:build');
	});
});

gulp.task('webserver', function ()
{
	browserSync(config);
});

gulp.task('clean', function (cb)
{
	rimraf(path.clean, cb);
});

gulp.task('zip', function ()
{
	return gulp.src('./build/**/*')
		.pipe(zip('build.zip', {compress: false}))
		.pipe(gulp.dest('./dist/'));
});

gulp.task('default', ['sprite', 'build', 'webserver', 'watch']);

// gulp clean -> gulp build -> gulp sprite:build -> gulp zip
