;
$(function ()
{
	var slider = $('#js-product-slider');
	var bxProductSlider = slider.bxSlider({
		responsive  : true,
		controls    : false,
		auto        : !!(slider.attr('data-bxAuto')) || false,
		pagerCustom : slider.attr('data-bxPagerCustom') || false,
		onSlideAfter: function ()
		{
			zoomer();
		},
		onSliderLoad: function ()
		{
			zoomer()
		}

	});

	$('#js-product-imageZoomCall').on('click', function (e)
	{
		$.fancybox({
			padding: 2,
			'href' : slider.getCurrentSlideElement().children('img').attr('data-bigImageSource')
		});
	});
	function zoomer()
	{
		var link = $('#zoom_zoom'),
		    index = slider.getCurrentSlide();

		if (typeof link == 'object' && link.attr('data-sliderIndex') == index)
			return false;
		else
			$('#zoom_zoom').remove();
		var image = slider.getCurrentSlideElement().children('img');

		link = $('<a id="zoom_zoom" class="b-product-gallery-zoom_zoom"></a>').attr({
			href             : image.attr('data-bigImageSource'),
			'data-large-url' : image.attr('data-bigImageSource'),
			'data-slideIndex': index
		});
		link.append(image.clone());
		link.appendTo('#js-slider-inner');
		link.jqzoom({
			zoomType    : 'reverse',
			lens        : true,
			alwaysOn    : false,
			zoomWidth   : 500,
			zoomHeight  : image.height(),
			imageOpacity: 0.3,
			xOffset     : 70,
			yOffset     : 0,
			preload     : 1,
			title       : false,
			position    : 'right',
			preloadText : 'Загрузка...'
		});
	}
});
