;

(function (jQuery)
{
	jQuery.fn.hasAttr = function (name)
	{
		for (var i = 0, l = this.length; i < l; i++)
		{
			if (!!( this.attr(name) !== undefined ))
			{
				return true;
			}
		}
		return false;
	};
})(jQuery);
$(function ()
{
	$('[data-toggle="tooltip"]').tooltip();

	$(".js-modal__call").click(function (e)
	{
		e.preventDefault();
		$($(this).attr('data-call')).arcticmodal({
			overlay: {
				css: []
			},
			clone  : true
		});
	});

	$(".js-ajax_modal__call").click(function (e)
	{
		e.preventDefault();
		$.arcticmodal({
			type   : 'ajax',
			url    : this.href,
			overlay: {
				css: []
			}
		});
	});

	$('body').on("click", '.js-checkbox, .js-radio', function (e)
	{
		e.stopPropagation();
		e.preventDefault();

		var input = $(this).find('input[type="' + ($(this).hasClass('js-radio') ? 'radio' : 'checkbox') + '"]');

		if ($(this).hasClass('js-radio'))
		{
			$('input[type="radio"]').filter('[name="' + (input.attr('name') || '') + '"]').each(function ()
			{
				$(this).attr('checked', false);
				$(this).parents('.js-checkbox, .js-radio').removeClass('checked')
			});
		}
		label = $(this).toggleClass('checked');
		input.attr('checked', (label.hasClass('checked') ? 'checked' : false));

	}).on('click', '.js-toggle-link', function (e)
	{
		e.preventDefault();
		var toggled = $(this).attr('data-toggled');
		$(this).parents('*:has(' + toggled + '):eq(0)').toggleClass('state-open').find(toggled).slideToggle(200);

	}).on('click', '.js-clone-form', function (e)
	{
		e.returnValue = false;
		var row = $($(this).attr('href'));
		var counter = row.parent().children('.form-row:not(' + $(this).attr('href') + ',.form-clone_row)').size() + 1;
		if (row.size() > 0)
		{
			var clone = row.clone();
			row.before(clone.html(clone.removeAttr('id').removeClass('hide')[0].innerHTML.replace(/%increment%/g, counter)));
		}
	});

	$('.js-checkbox, .js-radio').each(function (i, e)
	{
		if ($(this).find('input:checked').size() > 0)
		{
			$(this).addClass('checked');
		}
	});

	var slectbox = [];
	if ((selectbox = $('select.js-custom-select')) && selectbox.length > 0)
	{
		selectbox.ikSelect({
			syntax     : '<div class="ik_select_link"><div class="ik_select_link_inner"><span class="ik_select_link_text f-left"></span><i class="b-icon-32 b-icon-arrow_down__red ik_select_dropdown_icon"></i></div></div><div class="ik_select_dropdown"><div class="ik_select_list"></div></div>',
			customClass: 'b-form-custom_select__js',
			autoWidth  : false,
			ddFullWidth: false,
			ddMaxHeight: 300,
			filter     : true
		});
	}

	var slideshow = [];
	if ((slideshow = $('.js-slideshow')) && slideshow.length > 0)
	{
		slideshow.each(function (i, _slider)
		{
			$(this).bxSlider({
//				responsive  : true,
				pager       : !!($(_slider).attr('data-bxPagerCustom')) || false,
				auto        : !!($(_slider).attr('data-bxAuto')) || false,
				pagerCustom : $(_slider).attr('data-bxPagerCustom') || false,
				nextSelector: $(_slider).attr('data-bxNextSelector') || null,
				prevSelector: $(_slider).attr('data-bxPrevSelector') || null,
				nextText    : $(_slider).attr('data-bxNextText') || 'Следующий',
				prevText    : $(_slider).attr('data-bxPrevText') || 'Предыдущий',
				onSliderLoad: function (e)
				{

				}
			});
		});
	}

	/* Widget video. Insert custom iframe for embeded video, from youtube */
	$('[data-video_embed]').each(function ()
	{
		$(this).html('<iframe src="//www.youtube.com/embed/' + $(this).attr('data-video_embed') + '?rel=0&fs=0&showinfo=0&controls=0&autohide=1" frameborder="0" allowfullscreen></iframe>');
	});

	/* Widget maps. Find "data-api" call elements and insert custom google map */
	var maps = [];
	if ((maps = $('[data-maps]')) && (maps.length > 0))
	{
		maps.each(function (i, map)
		{
			var coordinates = $(map).attr('data-maps_coordinates').split(';');
			if (typeof coordinates == 'object' && coordinates !== null && coordinates.length >= 1)
			{
				var centerPos = coordinates[0].split(',');
				var markerPos = (coordinates[1] || coordinates[0]).split(',');
				this.id = this.id || 'mapID_' + Math.random();

				if ($(map).data('maps') == 'yandex' && 'ymaps' in window)
				{
					ymaps.ready(function ()
					{
						var myMap = new ymaps.Map(map.id, {
							center   : [centerPos[0], centerPos[1]],
							zoom     : $(map).attr('data-maps_zoom') || 14,
							controls : [],
							behaviors: ['drag', 'scrollZoom', 'multiTouch']
						});

						myMap.geoObjects.add(new ymaps.Placemark([markerPos[0], markerPos[1]], {
							hintContent: $(map).attr('data-maps_marker_title') || 'Мы тут'
						}, {
							preset: 'islands#redDotIcon'
						}));
					});

				}
				else if ($(this).data('maps') == 'google' && 'google' in window)
				{
					mapOptions = {
						zoom             : $(this).attr('data-maps_zoom') || 14,
						center           : new google.maps.LatLng(centerPos[0], centerPos[1]),
						mapTypeControl   : false,
						mapTypeId        : google.maps.MapTypeId.ROADMAP,
						zoomControl      : false,
						streetViewControl: false
					};
					var map = new google.maps.Map(this, mapOptions),
					    marker = new google.maps.Marker({title: $(this).attr('data-maps_marker_title') || 'Мы тут'});
					marker.setMap(map);
					marker.setPosition(new google.maps.LatLng(markerPos[0], markerPos[1]));
					marker.setVisible(true);
				}
			}
		});
	}

	/* TABS ENGINE */
	$('.b-tabs-content .b-tabs-content-item:not(.active)').css({display: 'none'});
	$(window).bind('hashchange', function (e)
	{
		if (this.location.hash)
		{
			var tabContent = $('.b-tabs-content').find(this.location.hash),
			    link = $('.b-tabs').find('.b-tabs-controls-item a[href="' + this.location.hash + '"]'),
			    scrollLocation = this.scrollTop;

			e.preventDefault();

			tabContent.parents('.b-tabs-content').children('.b-tabs-content-item').removeClass('active').hide(1,
				function ()
				{
					tabContent.addClass('active').css({display: 'block'});
				}
			);

			link.parents('.b-tabs-controls').children('.b-tabs-controls-item').removeClass('active');
			link.parent('.b-tabs-controls-item').addClass('active');

			$(this).animate({scrollTop: scrollLocation}, 300);
		}
		return false;
	}).trigger('hashchange');

	var carousel = $('.js-carousel');
	if (carousel.length > 0)
	{
		carousel.bxSlider({
			responsive  : false,
			onSliderLoad: function (e)
			{
				slideshow.parent().css({height: 'auto'});
			}
			,
			pager       : false,
			minSlides   : 8,
			maxSlides   : 8,
			slideWidth  : 36,
			slideMargin : 15
			//slideWidth:
		})
	}

	var __vote = $('.js-vote');
	if (__vote.length > 0)
	{
		__vote.each(function ()
		{
			if ($(this).hasAttr('data-voteReadonly')) $(this).prop('title', 'Read only');
			$(this).raty({
				path    : 'i/src/vote',
				half    : $(this).hasAttr('data-voteHalf'),
				score   : $(this).attr('data-voteScore') || 0,
				readOnly: $(this).hasAttr('data-voteReadonly'),
				hints   : ['1', '2', '3', '4', '5']
			});
		});
	}
});
