<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="ru"><![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="ru"><![endif]-->
<!--[if IE 9]>
<html class="ie ie9 gte-ie9" lang="ru"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js gt-ie9" lang="en"> <!--<![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<meta charset="utf-8"/>
		<title> - Ester Furs</title>
		<link rel="stylesheet" href="css/main.css?132=1">
	</head>
	<body class="b-page">
		<div class="b-page-wrapper">
			//= ./templates/_header.html
			<main class="container" role="main">
				<!-- // = ./templates/_breadcrumbs.html -->
				<div class="row b-page-content">
					<aside class="col-sm-2 b-page-sidebar">
						//= ./templates/_sidebar.html
					</aside>
					<aside class="col-sm-10">
						<div class="b-heading b-heading__lines b-page-title">
							<span></span>
						</div>
					</aside>
				</div>
			</main>
		</div>
		//= ./templates/_footer.html
		//= ./templates/_scripts.html
	</body>
</html>
